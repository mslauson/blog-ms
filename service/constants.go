package service

const (
	POST             = "post"
	COMMENT          = "comment"
	POST_EXISTS      = "post already exists"
	NO_POST_FOUND    = "no post found"
	NO_COMMENT_FOUND = "no comment found"
)
